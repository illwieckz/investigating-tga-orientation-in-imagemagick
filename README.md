Investigating TGA orientation in ImageMagick
============================================

The present study is conducted by _Thomas “illwieckz” Debesse_ and is funded by [rebatir.fr](https://rebatir.fr) in order  to help the ImageMagick project to make the related tools better and more reliable. _rebatir.fr_ offers expertise in the architecture, reliability and rehabilitation of IT infrastructures.


History
-------

- `2021-06-21`: Debut of the study.
- `2021-07-06`: Initial release of the study and related tools and samples.
- `2021-07-12`: Minor fix in tools and samples for a swapped color channel without incidence on image orientation neither on the conclusion of the study.


Context
-------

The ImageMagick convert tool has suffered multiple bugs for years producing upside-down images when the TGA image file format is involved:

- `ImageMagick#1406`: https://github.com/imagemagick/imagemagick/issues/1406  
  `2018-12-05` – _tga to tiff gets rotatet_
- `ImageMagick#3061`: https://github.com/imagemagick/imagemagick/issues/3061  
  `2021-01-02` –  _TGA Converted Images are Disoriented_ (converted to JPG)
- `ImageMagick#3844`: https://github.com/imagemagick/imagemagick/issues/3844  
  `2021-06-19` – _A TGA image converted to PNG is flipped vertically._

This repository provides the present document, a set of tool and image samples as part of an independent study to demonstrate a bug in ImageMagic `convert` tool when converting TGA files.

The author of the study first faced the bug while converting JPG to TGA in 2019.

- `launchpad/imagemagick#1838860`: https://bugs.launchpad.net/ubuntu/+source/imagemagick/+bug/1838860  
  `2019-08-04` – _convert translates upside down some picture when converting from jpg to tga_

He implemented at the time a workaround in a third party software (Urcheon, an artistic asset builder for game development).

- `Urcheon@b9a42f6`: https://github.com/DaemonEngine/Urcheon/commit/b9a42f65117fd4a7fa59c72ba07c5e738aef90fb  
  `2019-08-04` – _workaround ImageMagick's jpg-to-tga convert bug, fix `#33`_

But an user of that Urcheon software discovered in 2021 another variant of the bug was affecting the same third-party software when converting from TGA to PNG (`ImageMagick#3844`).

Looking through the ImageMagick bug tracker, it was noticed other people had faced variants of this bug, for example while convertig from TGA to TIFF (`ImageMagick#1406`) and from TGA to JPG (`ImageMagick#3061`).

Instead of implementing another workaround, decision was made to fund the present study to track down the root cause of the bug in order to help the ImageMagick project to make the related tools better and more reliable.


Conclusion
----------

After investigation, the bug comes from the fact ImageMagick mistakenly uses the storage format convention as a metadata and adds it to the metadata coding the image orientation and because of this, the ImageMagick `convert` tool does not preserve the orientation.


File naming conventions
-----------------------

The “_bottom_”, “_top_”, “_left_”, “_right_” words in samples file names are referring to the origin of the display the first bytes in TGA bitstream are expected to be written to. The “_bottom_” word does not mean the vertical orientation of the image has to be reverted. Some other image formats may use other storage conventions, endianness, channel ordering, etc. and the naming convention of this document is based on the TGA specification. What is called a “bottom left” TGA image may be called a “top left” image in a different image file format specification that is not TGA.


Understanding the required concepts
-----------------------------------

The root cause of those bug seems to be a confusion about semantic of data ordering (the way the image data is stored and decoded) and orientation metadata (non-image data that may code an image transformation to be done after decoding the image data itself).

To help identify what is data ordering and what is orientation metadata, let's figure a neutral screen with a projector that is a decoder for one format and a camera that is a coder for another format. Both projector and camera, hence boths decoder and coder, displays and captures from the same screen, hence writes to and reads from the same buffer. The format of this buffer is common to both decoder and coder so we can ignore it and we do not define it for the experience.

```
 Neutral screen
 _____________________________________
[                                     ]
[                                     ]
[                                     ]
[               XXX---                ]
[               XXX---                ]
[               ------                ]
[               ------                ]
[                                     ]
[                                     ]
[                                     ]
[_____________________________________]







         \     /
          \___/          _____
     _____|___|___      |     |
    |             |      |   |
    |             |      |___|  __
    |             |    _/     \/  |
    |             |   |           |
    |_____________|   |___________|

     TGA projector     PNG Camera
```

Default TGA file format without transformation data:

```
------------XXX---XXX---
```

Default PNG file format without transformation data (before compression):

```
XXX---XXX---------------
```

Those file formats can be _represented_ this way:

```
   Default TGA        Default PNG
   format without     format without
   transformation     transformation
   data               data

       ------             XXX---
       ------             XXX---
       XXX---             ------
       XXX---             ------
```

This is a _representation_ of the data an hexadecimal editor may provide, given the column number matches. This is _does not represent_ the image, _neither represent_ the orientation of the image.

Those both formats are coding the exact same image:

```
       XXX---
       XXX---
       ------
       ------
```

If the TGA file has the vertical and the horizontal bits unset (equal to zero), and a TGA decoder displays this image, the TGA decoder has a bug:

```
       ------
       ------
       XXX---
       XXX---
```

If the TGA file has the vertical and the horizontal bits unset (equal to zero), and a TGA to PNG converter produces a PNG without orientation metadata that displays this image, the converter has at least one bug either in the TGA decoder or in the PNG encoder:

```
       ------
       ------
       XXX---
       XXX---
```

The fact the first bytes of a TGA bit stream code the left pixel of the bottom line of the screen is a format convention, the fact another image format may code the top left pixel of the screen as first bytes of a PNG bit stream prior to compression is another format convention, none of these format differences are coding an orientation metadata.

This is very similar to what happens with big-endian and little-endian image formats. For example the PNG format has integers stored as big endian while TGA format has integers stored as little endian. A TGA or PNG decoder must respect the endianness even if it is chosen to not apply further transformation that may be coded in the file. This is true as well for the TIFF format that has both little-endian and big-endian formats.

For example producing a big endian or a little endian TIFF with ImageMagick with command `magick convert image.tga -define tiff:endian=msb image-msb.tif` or `magick convert image.tif -define tiff:endian=isb image-isb.tif` (without `-auto-orient`) must not alter colors. The `-auto-orient` option must not be required to get a file that would display expected colors when read by third party software.

In the same way, converting a TGA to PNG with ImageMagick command `magick convert image.tga image.png` must not require `-auto-orient` to produce an image that has first bytes of the TGA bitstream coding the left pixel of the bottom line in the PNG file if the orientation bits are unset (equal to zero).

Similar problematic may be faced with formats describing pixels as RGBA streams or as BGRA or ARGB ones or other variants. A device ignoring orientation metadata must not swap the color channels and adjust the byte read or the pixel display accordingly to the specification of the given format.

The TGA specification makes non-ambiguous usage of the “_display_” verb when describing the decoding process, and makes non-ambiguous usage of the “_display device_” terminology.

- `TGA specification`: https://www.dca.fee.unicamp.br/~martino/disciplinas/ea978/tgaffs.pdf  
  `1991-01` – _Truevision TGA File Format Specification Version 2.2_ (Authored by _Truevision, Inc._, provided by the _Faculdade de Engenharia Elétrica e de Computação_)

A TGA decoder must display first TGA image bytes as pixel on bottom left on the “_display device_” even when ignoring possible metadata telling the contrary.

Some other image formats like the BMP format also stores data in rows in a way the first bytes are expected to be displayed starting from bottom left. This format convention does not code image orientation metadata.

The BMP specification makes use of the non-ambiguous “_target device_” terminology.

- `BMP header specification`: https://docs.microsoft.com/en-us/windows/win32/api/wingdi/ns-wingdi-bitmapcoreheader  
  `2018-05-12` – _BITMAPCOREHEADER structure (wingdi.h)_ (Authored and provided by _Microsoft Corporation_)

A BMP decoder must display first BMP image bytes as pixel on bottom left of the “_target device_” if negative sign bit is not set in height field, and a TGA decoder must display first TGA image bytes as pixel on bottom left of the “_display device_” if the vertical orientation bit is unset in attribute field. The sign bit in BMP height field or the lack of vertical orientation bit in TGA attribute field being left unset to 0 does not code a vertical flip metadata of the image.

After the TGA file is written to the buffer from bottom left to top right, an encoder for another format like the PNG format may then start to read the buffer from top left to bottom right to store the first image bytes if such format like the PNG format stores top left pixel as first bytes of the bitstream before compression.


The ImageMagick TGA bug
-----------------------

When reading a TGA file with orientation bits unset (equal to zero), ImageMagick tools report a `BottomLeft` origin and use this information as an orientation metadata to transform the image, which contradicts the fact the orientation bits are unset.

This is because the TGA storage format _can be represented in a paper_ as starting with “bottom left” if the default _representation in the same paper_ is chosen to represent image data storage starting with “top left”.

For example, an user reading the data with a tool without knowledge of the format, like an hexadecimal editor, would see the data from the same pixels at different places given the file format is used. This is similar to the fact bytes or bits may be ordered differently.

The bug is that TGA image data format description is used to set the `BottomLeft` orientation while this task must left to the dedicated metadata.

Then the metadata information reverts the image that is already mistakenly reverted, so TGA image without orientation bit set are displayed upside-down, and TGA images with vertical orientation bit set are displayed as if the vertical orientation bit was ignored.

So the ImageMagick tools mistakenly uses the image data format description (that does not code display orientation) as a metadata by itself and adds it to the metadata (that code displays information) which produce misoriented files on the vertical axis. The usage of the `-auto-orient` option may produce files that display properly because it's like flipping vertically two times by mistake, each flip mutually canceling themselves.

That bug is tracked in [`ImageMagick#3844`](https://github.com/imagemagick/imagemagick/issues/3844).

There is another bug in ImageMagick `convert` tool related to this semantic misinterpretation, like producing a non-default format by writing the file upside-down and setting a vertical orientation bit to restore the orientation at display time when converting to TGA file format, even when the original file in another format did not carried any orientation information.

That other bug is tracked there:

- `ImageMagick#3903`: https://github.com/ImageMagick/ImageMagick/issues/3903  
  `2021-06-06` – _ImageMagick convert tool writes a non-default TGA format and sets a metadata to tell third-party tools to adjust_


The produce-reference-tga tool
------------------------------

The [`produce-reference-tga`](produce-reference-tga) file is a tool to produce TGA files in a “_what you write is what you get_” reproduceable way in order to compare visual results without bias.

That tool is written in Python 3 language and does not make use of external package.

Multiple options and presets are selectable by editing the source code itself.

The sample TGA images can be produced by calling the tool this way:

```sh
./produce-reference-tga
```

The image to be produced is described as a string array in order to easily compare what is wanted to be produced and how the image may be displayed or converted by third-party tools like `display` or `convert`.

```
┌────────────────────────────────┐
│ ░▒▓█RGBA███████                │
│████████████████                │
│██      ████████                │
│██  ████████████                │
│██ █ ███████████                │
│██ ██ ██████████                │
│██ ███ █████████                │
│██ ████ ████████                │
│████████████████                │
│████████████████                │
│████   ██ ██████                │
│█████ ███ ██████                │
│█████ ███ ██████                │
│█████ ███   ████                │
│████████████████                │
│███████ ░▒▓█RGBA                │
│                                │
│                                │
│                                │
│                                │
│                                │
│                                │
│                                │
│                                │
│                                │
│                                │
│                                │
│                                │
│                                │
│                                │
│                                │
│                                │
└────────────────────────────────┘
```

That string array defining the image is accompanied by a text written in english language describing how the image must be displayed when displayed correctly.

> The image is a square of 32 lines and 32 columns with RGBA data, each pixel
> having 4 channels.  
> It depicts a white large area with a black square on top left corner
> containing a white arrow on the third row and sixth column pointing to the
> top left corner and, under the arrow, the two capital T and L latin letters
> for Top and Left.  
> The T and L letters must be properly orientated with the horizontal bar of
> the T being horizontal and on top of the glyph and the vertical bar of the L
> being vertical and on left of the glyph. The T and L letters must be on the
> same line and the T letter must be on the left of the L letter.  
> On the first line, from left to right, there must be single pixels being
> subsequently white, light grey, medium grey, dark grey, black, red, green,
> blue, and a completely transparent pixel. The same pattern from white to
> black, colored and transparent pixels is reproduced in the same order at
> the bottom right of the black square.

The produced image or converted files may display in a way that does not contradict the string array representation (given the text is printed from top left to bottom right) and the description in english language.

When converting [`samples/reference-bottom-left.tga`](samples/reference-bottom-left.tga) to another format like the PNG format, the arrow must point to the top left on the screen and the `T` and `L` latin letters be readable in the latin read orientation and order.

Additional files are provided in PNG format like the [`samples/from-bottom-left.png`](samples/from-bottom-left.png) one, which is an additional way to compare with what third-party converters may produce:

[![PNG sample](samples/from-bottom-left.png)](samples/from-bottom-left.png)

The TGA files are produced with metadata description field reproducing their names and extra padding to align the produced data on columns in a way third-party hexadecimal editor can display the bitstream as columns and rows.

A [`hexdump.cfg`](hexdump.cfg) configuration file for the `hexdump` tool is provided in a way an user can look at the produced bitstream this way:

```sh
hexdump -v -f hexdump.cfg samples/reference-bottom-left.tga
```

The `hexdump` representation must be upside down compared to the text representation and the display of the `samples/reference-bottom-left.tga` image.

A reference of this output is provided in the `produce-reference-tga` source code.


The convert-with-magick tool
----------------------------

Another tool named [`convert-with-magick`](convert-with-magick) is provided to convert TGA files to PNG and BMP without using ImageMagic `convert` tool and the `-auto-orient` option, to help compare the produced files with references and track down bugs. This tool is written in bourne shell.

The converted PNG and BMP image files can be produced by calling the tool this way:

```sh
./convert-with-magick
```


TGA file samples
----------------

The TGA files produced with the “_default_” preset can be found for each orientation in the [`samples/`](samples/) folder with names starting with `reference-` prefix and for every orientation a properly converted PNG file is provided (with `from-` prefix) to help figure out bugs in third-party conversion software.

When converting TGA to PNG without taking care of orientation metadata, the [`samples/reference-bottom-left.tga`](samples/reference-bottom-left.tga) and the [`samples/from-bottom-left.png`](samples/from-bottom-left.png) file must look exactly the same.

When converting TGA to PNG while taking care of orientation metadata, the `samples/reference-bottom-left.tga` and the `samples/from-bottom-left.png` file must look exactly the same and all the other PNG files must look exactly the same.

TGA files produced with the “_correct-magick_” preset can be found for every orienation with names starting with `raw-` prefix. Files produced with ImageMagick `convert` tool are expected to be strictly equal byte-to-byte as long as the ImageMagick tool has no bug.

The `samples/reference-bottom-left.tga` file must always display an arrow pointing to top left corner of the image after conversion to another image file format, when stripping orientation metadata or not.

The TGA files produced while not setting the orientation bits (and PNG files properly converted from them) can be found for each orientation in [`samples-strip/`](samples-strip/) folder and names make use of the same prefixes.


Analysis
--------

At the time of the study, it appears that a TGA file with metadata to flip vertically the image being unset (meaning no vertical flip has to be done) is flipped vertically when converted to PNG using ImageMagick's `convert` tool, as seen here in XnView MP image viewer:

[![TGA to PNG vertical flip](attached/screenshot-tga-png-vertical-flip.png)](attached/screenshot-tga-png-vertical-flip.png)

_On the left, the reference TGA using bottom-left storage (according to specification) with bottom-left orientation (default orientation according to specification). On the right, a PNG produced by converting the TGA file using ImageMagick `convert` tool without using any image transformation option. Both TGA and PNG files have metadata unset._

Because the image source has orientation metadata unset, it is expected the converted image should not have orientation modified. Neither the orientation metadata should be modified to tell image viewers to flip vertically the image, neither the image must be modified by flipping the pixels vertically.

Here is the PNG image the ImageMagick `convert` tool should produce when converting the `reference-bottom-left.png` file:

[![Correctly converted PNG image from TGA sample](samples/from-bottom-left.png)](samples/from-bottom-left.png)

Here is the PNG image the ImageMagick `convert` tool produces when converting the `reference-bottom-left.png` file at the time of this study:

[![Incorrectly converted PNG image from TGA sample](convert/from-bottom-left-no-transformation.png)](convert/from-bottom-left-no-transformation.png)

It appears the ImageMagick `convert` tool to not preserve the orientation of how it was stored.

It appears the ImageMagick `convert` tool mistakenly uses the storage format convention as a metadata and adds it to the metadata coding the image orientation and because of this, the ImageMagick `convert` tool does not preserve the orientation.


Author
------

- Thomas “illwieckz” Debesse \<dev `(ad)` illwieckz.net\>


Licensing
---------

The present document, the source code of the tools and the provided samples are covered by the CC0 1.0 Universal (CC0 1.0) ”_Public Domain Dedication_” license, as described in the [`LICENSE.md`](LICENSE.md) file or on this page:

- https://creativecommons.org/publicdomain/zero/1.0/legalcode  
  _Creative Commons CC0 1.0 Universal_
